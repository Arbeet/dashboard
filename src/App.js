import { ColorModeContext, useMode } from "./theme";
import { CssBaseline, ThemeProvider } from "@mui/material";
import { Routes, Route } from "react-router-dom";
import Topbar from "./scenes/global/Topbar"
import Dashboard  from "./scenes/dashboard/index";
import Sidebar from "./scenes/global/Sidebar";
import Team from "./scenes/team";
import Contact from "./scenes/contacts";
import Invoice from "./scenes/invoices";
import Form from "./scenes/form";
import Calendar from "./scenes/calendar";
import Bar from "./scenes/Bar";



function App() {
  const [theme, colorMode] = useMode();
  return (
    <ColorModeContext.Provider value={colorMode}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
      <div className="app">
        <Sidebar />
        <main className="content">
          <Topbar />
          <Routes>
            <Route path="/" element={<Dashboard />}/>
            <Route path="/team" element={<Team />} />
            <Route path="/contacts" element={<Contact />} />
            <Route path="/invoices" element={<Invoice />} />
            <Route path="/form" element={<Form />} />
            <Route path="/calendar" element={<Calendar />} />
            <Route path="/bar" element={<Bar />} />
          </Routes>
        </main>
      </div>
      </ThemeProvider>
    </ColorModeContext.Provider>
  );
}

export default App;
